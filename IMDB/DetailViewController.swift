//
//  DetailViewController.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var movieData:MovieDataResponse!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var plotLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = movieData.movieData.name
        plotLabel.text = "Plot: \(movieData.movieData.plot)"
        directorLabel.text = "Director: \(movieData.movieData.director)"
        starsLabel.text = "Stars: \(movieData.movieData.stars)"
        posterImageView.image = Utility.imageFromUrlString(url: movieData.movieData.poster_url)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
