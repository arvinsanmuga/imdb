//
//  IMDBService.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import PromiseKit

class IMDBService {
    
    class func getMovieData(keyword:String) -> Promise<MovieDataResponse> {
        
        return Promise { seal in
            
            let url = "http://theapache64.com/movie_db/search?keyword=\(keyword)"
            Alamofire.request(url).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let responseJSON = JSON(value)
                    
                    let movieDataResponse = MovieDataResponse(movieData: MovieData(name:responseJSON["data"]["name"].stringValue,
                                                                                   director: responseJSON["data"]["director"].stringValue,
                                                                                   imdb_id: responseJSON["data"]["imdb_id"].stringValue,
                                                                                   genre: responseJSON["data"]["genre"].stringValue,
                                                                                   year: responseJSON["data"]["year"].stringValue,
                                                                                   stars: responseJSON["data"]["stars"].stringValue,
                                                                                   plot: responseJSON["data"]["plot"].stringValue,
                                                                                   poster_url: responseJSON["data"]["poster_url"].stringValue),
                                                              error_code: responseJSON["error_code"].intValue,
                                                              message: responseJSON["message"].stringValue,
                                                              error: responseJSON["error"].boolValue)
                    
                    seal.fulfill(movieDataResponse)
                    
                case .failure(let error):
                    print(error)
                    seal.reject(error)
                }
            }
        }
    }
}
