//
//  MovieData.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import Foundation

struct MovieDetail {
    var name:String
    var imageurl:String
}

struct MovieDataResponse {
    
    var movieData:MovieData!
    var error_code = 0
    var message = ""
    var error = false
}

struct MovieData {
    var name = ""
    var director = ""
    var imdb_id =  ""
    var genre = ""
    var year =  ""
    var stars = " "
    var plot = ""
    var poster_url = ""
}

