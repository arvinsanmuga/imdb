//
//  Utility.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit
import SwiftyJSON

class Utility {
    
    class func imageFromUrlString(url:String) -> UIImage {
        let data = try? Data(contentsOf:URL(string:url)!)
        return UIImage(data: data!)!
    }
    
    class func readJSONFileFromPath(forResource:String, ofType:String) -> JSON {
        let path = Bundle.main.path(forResource: forResource, ofType: ofType)!
        let jsonString = try? String(contentsOfFile: path, encoding: String.Encoding.utf8)
        return JSON(parseJSON: jsonString!)
    }
}

func generateRandomData() -> [[UIColor]] {
    let numberOfRows = 20
    let numberOfItemsPerRow = 15
    
    return (0..<numberOfRows).map { _ in
        return (0..<numberOfItemsPerRow).map { _ in UIColor.randomColor() }
    }
}

extension UIColor {
    
    class func randomColor() -> UIColor {
        
        let hue = CGFloat(arc4random() % 100) / 100
        let saturation = CGFloat(arc4random() % 100) / 100
        let brightness = CGFloat(arc4random() % 100) / 100
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
}
