//
//  VerticalCollectionViewCell.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

class VerticalCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
