//
//  VerticalTableViewCell.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

protocol VerticalCellDelegate: class {
    func showDetailViewController(selectedMovie: MovieDetail)
}

class VerticalTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate: HorizontalCellDelegate!
    var list :[MovieDetail] {
        get { return readList() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self as UICollectionViewDataSource
        collectionView.delegate = self as UICollectionViewDelegate
        
        let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionViewLayout?.sectionInset = UIEdgeInsetsMake(10, 10, 20, 10)
        collectionViewLayout?.invalidateLayout()
    }
    
    func readList() -> [MovieDetail] {
        let json = Utility.readJSONFileFromPath(forResource: "MovieList", ofType: "json")
        
        var shoelist = [MovieDetail]()
        for shoe in json["movies"].arrayValue {
            shoelist.append(MovieDetail(name: shoe["name"].stringValue,
                                        imageurl: shoe["imageurl"].stringValue))
        }
        
        return shoelist
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VeCell", for: indexPath) as! VerticalCollectionViewCell
        
        DispatchQueue.main.async {
            cell.titleLabel.text = self.list[indexPath.row].name
            cell.posterImageView.image = Utility.imageFromUrlString(url:self.list[indexPath.row].imageurl)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.showDetailViewController(selectedMovie: list[indexPath.row])
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
}
