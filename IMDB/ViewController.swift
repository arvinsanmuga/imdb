//
//  ViewController.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 13/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var moviesTableView: UITableView!
    var viewModel = ViewModel()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(ViewController.handleRefresh(_:)), for:.valueChanged)
        refreshControl.tintColor = .black
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moviesTableView.addSubview(self.refreshControl)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.moviesTableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue" {
            let viewcontroller = segue.destination as! DetailViewController
            viewcontroller.movieData = sender as! MovieDataResponse
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HoCell", for: indexPath) as! HorizontalTableViewCell
            cell.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VeCell", for: indexPath) as! VerticalTableViewCell
            cell.delegate = self
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 120 : 500
    }
}

extension ViewController: HorizontalCellDelegate, VerticalCellDelegate {
    
    func showDetailViewController(selectedMovie: MovieDetail) {
        
        let encodedKeyword = viewModel.addPercentEncoding(text: selectedMovie.name)
        viewModel.getMovieData(keyword: encodedKeyword).done { (response) in
            print(response)
            self.performSegue(withIdentifier: "segue", sender: response)
            }.catch { (error) in
                print(error)
        }
        
        
    }
}

