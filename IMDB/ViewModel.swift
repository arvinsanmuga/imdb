//
//  ViewModel.swift
//  IMDB
//
//  Created by Arvin Sanmuga Rajah on 18/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import Foundation
import PromiseKit

class ViewModel {
    
    func addPercentEncoding(text:String) -> String {
        return text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    func getMovieData(keyword: String) -> Promise<MovieDataResponse>  {
        return Promise { seal in
            IMDBService.getMovieData(keyword: keyword).done { (response) in
                seal.fulfill(response)
                }.catch { (error) in
                    seal.reject(error)
            }
        }
    }
}

