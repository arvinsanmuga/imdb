//
//  IMDBTests.swift
//  IMDBTests
//
//  Created by Arvin Sanmuga Rajah on 13/06/2018.
//  Copyright © 2018 Arvin Sanmuga Rajah. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyJSON
import PromiseKit
@testable import IMDB

class IMDBTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPercentEncoding() {
        let viewModel = ViewModel()
        let encodedKeyword = viewModel.addPercentEncoding(text: "John Wick")
        if encodedKeyword != "John Wick" {
            XCTAssert(true)
        }
    }
    
    func testGetMovieDatafromIMDBService() {
         let e = expectation(description: "ViewModel")
        
        let viewModel = ViewModel()
        let encodedKeyword = viewModel.addPercentEncoding(text: "John Wick")
        viewModel.getMovieData(keyword: encodedKeyword).done { (response) in
            e.fulfill()
            XCTAssert(true)
            }.catch { (error) in
                e.fulfill()
                XCTFail()
        }
        
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
